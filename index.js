// Quizz 1:
// 1. What is the blueprint where objects are created from?
	// - Classes

// 2. What is the naming convention applied to classes?
	// - class names should begin with an uppercase letter

// 3. What keyword do we use to create objects from a class?
	// - new keyword

// 4. What is the technical term for creating an object from a class?
	// - Instantiate

// 5. What class method dictates HOW objects will be created from that class?
	// - class constructor


// Quiz 2:
// 1. Should class methods be included in the class constructor?
	// - No
// 2. Can class methods be separated by commas?
	// - No
// 3. Can we update an object’s properties via dot notation?
	// - Yes
// 4. What do you call the methods used to regulate access to an object’s properties?
	// - Setter Getter
// 5. What does a method need to return in order for it to be chainable?
	// - this




// Function Coding
class Student {
	
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.grades = grades.every(grade => (grades >= 0 && grades <= 100))? grades : undefined;
	}
	login(){
		console.log(`${this.name} has logged in`);
		return this;
	}
	logout(){
		console.log(`${this.name} has logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve(){
        this.gradeAve = (this.grades.reduce((a, b) => a + b) / this.grades.length)
        return this
    }
	willPass() {
        this.computeAve()
        this.passed = this.gradeAve >= 85
        return this
    }
	willPassWithHonors() {
        this.willPass()
        if(this.passed){
            if (this.gradeAve >= 90){
                this.passedWithHonors = true
            }else {
                this.passedWithHonors = false;
            }
        } else this.passedWithHonors = undefined
        return this
    }
}



let studentOne = new Student("John", "john@email.com", [89, 84, 78, 88]);
let studentTwo = new Student("Jane", "jane@email.com", [87, 89, 91, 93]);
let studentThree = new Student("Joe","joe@email.com", [78, 82, 79, 85])
let studentFour = new Student("Jessie", "jessie@email.com", [91, 89, 92, 93])







